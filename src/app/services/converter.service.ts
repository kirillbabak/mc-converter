import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "@environments/environment";
import { CURRENCY } from "@app/models/currency.enum";
import { map } from "rxjs/operators";

enum ENDPOINTS {
  latest = "/latest"
}

@Injectable({
  providedIn: "root"
})
export class ConverterService {
  constructor(private http: HttpClient) {}

  convert(from: CURRENCY, to: CURRENCY, amount: number) {
    return this.http
      .get(environment.api + ENDPOINTS.latest, {
        params: { base: from }
      })
      .pipe(map(({ rates }: { rates: CURRENCY[] }) => amount * rates[to]));
  }
}
