import { AmountCurrency } from "@app/models/amountCurrency.model";

export interface Conversion {
  from: AmountCurrency;
  to: AmountCurrency;
}
