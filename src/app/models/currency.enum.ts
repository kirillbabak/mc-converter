export enum CURRENCY {
  USD = "USD",
  CZK = "CZK",
  EUR = "EUR",
  JPY = "JPY",
  GBP = "GBP"
}
