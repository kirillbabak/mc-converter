import { CURRENCY } from "@app/models/currency.enum";

export interface AmountCurrency {
  amount: number;
  currency: CURRENCY;
}
