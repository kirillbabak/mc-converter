import { Component, OnInit, ChangeDetectionStrategy, Input } from "@angular/core";
import { Conversion } from "@app/models/conversion.model";

@Component({
  selector: "mc-converter-table",
  templateUrl: "./converter-table.component.html",
  styleUrls: ["./converter-table.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ConverterTableComponent implements OnInit {
  displayedColumns: string[] = ["from", "to"];

  @Input()
  conversions: Conversion[] = [];

  get totalCost() {
    return this.conversions.reduce((prev, current) => prev + current.to.amount, 0);
  }

  ngOnInit() {}
}
