import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConverterTableComponent } from './converter-table.component';

describe('ConverterTableComponent', () => {
  let component: ConverterTableComponent;
  let fixture: ComponentFixture<ConverterTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConverterTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConverterTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
