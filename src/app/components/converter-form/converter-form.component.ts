import { Component, OnInit, Output, EventEmitter, OnDestroy } from "@angular/core";
import { CURRENCY } from "@app/models/currency.enum";
import { FormBuilder, Validators } from "@angular/forms";
import { debounceTime, switchMap, filter, catchError, tap } from "rxjs/operators";
import { ConverterService } from "@app/services/converter.service";
import { of } from "rxjs";
import { MatSnackBar } from "@angular/material/snack-bar";
import { Conversion } from "@app/models/conversion.model";
import { untilDestroyed } from "ngx-take-until-destroy";

@Component({
  selector: "mc-converter-form",
  templateUrl: "./converter-form.component.html",
  styleUrls: ["./converter-form.component.scss"]
})
export class ConverterFormComponent implements OnInit, OnDestroy {
  @Output()
  submitForm = new EventEmitter<Conversion>();

  currencyOptions = [
    { label: "CZK", value: CURRENCY.CZK },
    { label: "EUR", value: CURRENCY.EUR },
    { label: "JPY", value: CURRENCY.JPY },
    { label: "GBP", value: CURRENCY.GBP }
  ];

  form = this.fb.group({
    amount: [null, [Validators.required, Validators.min(0)]],
    currency: [null, [Validators.required]]
  });

  loading = false;

  convertedAmount = 0;

  constructor(private fb: FormBuilder, private converterService: ConverterService, private matSnackBar: MatSnackBar) {}

  ngOnInit() {
    this.form.valueChanges
      .pipe(
        debounceTime(250),
        filter(() => this.form.valid),
        tap(() => (this.loading = true)),
        switchMap(({ amount, currency }) =>
          this.converterService.convert(currency, CURRENCY.USD, amount).pipe(
            catchError(() => {
              this.matSnackBar.open("Something went wrong", "OK");
              return of(null);
            })
          )
        ),
        untilDestroyed(this)
      )
      .subscribe(amount => {
        this.loading = false;
        this.convertedAmount = !!amount ? amount : 0;
      });
  }

  handleSubmit() {
    const { amount, currency } = this.form.value;

    if (!this.form.valid) {
      return this.matSnackBar.open("Form is invalid", "OK");
    }

    this.submitForm.emit({
      from: {
        amount,
        currency
      },
      to: {
        amount: this.convertedAmount,
        currency: CURRENCY.USD
      }
    });

    this.form.reset();
  }

  ngOnDestroy(): void {}
}
