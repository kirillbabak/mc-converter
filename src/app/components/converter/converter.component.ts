import { Component, OnInit } from "@angular/core";

@Component({
  selector: "mc-converter",
  templateUrl: "./converter.component.html",
  styleUrls: ["./converter.component.scss"]
})
export class ConverterComponent implements OnInit {
  conversions = [];

  constructor() {}

  ngOnInit() {}

  handleSubmit(conversion) {
    this.conversions = [...this.conversions, conversion];
  }
}
